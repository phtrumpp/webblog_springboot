package de.awacademy.springboot.services;

import de.awacademy.springboot.dto.BlogDTO;
import de.awacademy.springboot.entities.Blog;
import de.awacademy.springboot.repositories.BlogRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class BlogServiceTest {

    @Mock //Simulieren datenbankzugriff durch @Mock da wir in dieser Methode keinen Kontext haben
    private BlogRepository blogRepository;

    @InjectMocks //injezieren alle Mocks hier rein
    private BlogService blogService;

    @Before
    public void setUp() {
        //Stecken unsere Testklasse durch this hier rein
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createBlogs() {
        //1. Prepare test data
        List<BlogDTO> blogDTOS = new ArrayList<>();

        BlogDTO blogDTO1 = new BlogDTO();
        blogDTO1.setAuthor("Dennis");
        blogDTO1.setTitle("Biography");
        blogDTO1.setContent("lol");

        BlogDTO blogDTO2 = new BlogDTO();
        blogDTO2.setAuthor("Maher");
        blogDTO2.setTitle("Biography2");
        blogDTO2.setContent("lol2");

        BlogDTO blogDTO3 = new BlogDTO();
        blogDTO3.setAuthor("Alan");
        blogDTO3.setTitle("Biography3");
        blogDTO3.setContent("lol3");

        blogDTOS.add(blogDTO1);
        blogDTOS.add(blogDTO2);
        blogDTOS.add(blogDTO3);

        Blog blog = new Blog();
        when(blogRepository.save(any(Blog.class))).thenReturn(blog);

        //2. Run my business logic
        blogService.createBlogs(blogDTOS);

        //3. Verify the results
        verify(blogRepository, times(3)).save(any(Blog.class));
    }


}
