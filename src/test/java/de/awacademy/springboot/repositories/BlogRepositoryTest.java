package de.awacademy.springboot.repositories;

import de.awacademy.springboot.entities.Blog;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.AssertTrue;

import static org.junit.jupiter.api.Assertions.*;

/**
 * RunWith gives me a "fake" context which lives as long as the Test runs.
 * With this we can test our beans.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
class BlogRepositoryTest {

    @Autowired
    private BlogRepository blogRepository;

    @Test
    public void addBlog(){
        //1. Prepare test data
        Blog blog = new Blog();
        blog.setAuthor("Peter Lustig");
        blog.setTitle("Spring Boot Tutorial");
        blog.setContent("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, " +
                "sed diam nonumy eirmod tempor invidunt " +
                "ut labore et dolore magna aliquyam");

        //2. Run my logic
        blogRepository.save(blog); //Id ist automatisch gefüllt

        //3. Verify
        Long id = blog.getId();

        Blog copyBlog = blogRepository.findById(id).get();

        Assert.assertEquals(copyBlog.getId(),blog.getId());
        Assert.assertEquals(copyBlog.getTitle(),blog.getTitle());
        Assert.assertEquals(copyBlog.getContent(),blog.getContent());

    }

    @Test
    public void removeBlog(){

        Blog blog = new Blog();

        Blog savedBlog = blogRepository.save(blog);

        blogRepository.deleteById(savedBlog.getId());

        Assert.assertTrue(blogRepository.findById(savedBlog.getId()).isEmpty());


    }

}
