package de.awacademy.springboot.dto;

import lombok.Data;

// @Data = Lombok - generates -> getter, setter, toString
@Data
public class BlogDTO {

    private String author;
    private String title;
    private String content;
}
