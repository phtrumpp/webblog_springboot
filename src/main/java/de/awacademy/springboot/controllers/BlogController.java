package de.awacademy.springboot.controllers;

import de.awacademy.springboot.dto.BlogDTO;
import de.awacademy.springboot.services.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class BlogController {

    @Autowired
    private BlogService blogService;

    @PostMapping("/creation")
    public void addBlogs(@RequestBody List<BlogDTO> blogsDTOs) {
        // Durch die Annotations @RequestBody wird das JSON übergebene JSON gemappt
        System.out.println("--> addBlogs");

        // ToDo implement me
        blogService.createBlogs(blogsDTOs);

    }

    @GetMapping("/reading")
    public List<BlogDTO> readBlogs() {

        List<BlogDTO> blogDTOS = blogService.readAll();

        return blogDTOS;
    }
}
