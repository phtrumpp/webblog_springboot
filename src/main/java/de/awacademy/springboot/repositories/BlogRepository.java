package de.awacademy.springboot.repositories;

import de.awacademy.springboot.entities.Blog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Database repository to access spring-boot-db schema.
 *
 * Automatically implements CRUD methods by Spring.
 *
 * Repository interface go as bean via dependency injection
 * into the context. No @Repository annotation needed.
 * Anyway, we put one for clarity sake.
 */
public interface BlogRepository extends JpaRepository<Blog,Long> {

}
