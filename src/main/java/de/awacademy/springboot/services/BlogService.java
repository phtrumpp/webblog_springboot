package de.awacademy.springboot.services;

import de.awacademy.springboot.dto.BlogDTO;
import de.awacademy.springboot.entities.Blog;
import de.awacademy.springboot.repositories.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BlogService {

    @Autowired
    private BlogRepository blogRepository;

    // field injection ist evil - greift bei Tests (ohne fake Context) ins Leere. Annotation über Konstruktor ist optional!
    /*
    @Autowired
    public BlogService(BlogRepository blogRepository) {
           this.blogRepository = blogRepository;
    }

     */


    /**
     * Create multiple blog entries on the database.
     *
     * @param blogs List of BLogs
     */
    public void createBlogs(List<BlogDTO> blogs){
        for (BlogDTO blogDTO : blogs ) {

            blogRepository.save(mappingBlogEntity(blogDTO));

        }
    }


    
    
    public List<BlogDTO> readAll() {
        List<Blog> allBlog =  blogRepository.findAll();
        List<BlogDTO> allBlogDTO = new ArrayList<>();
        for (Blog blog: allBlog) {
            BlogDTO blogDTO = convertDTO(blog);
            allBlogDTO.add(blogDTO);
        }
        
        return allBlogDTO;
    }
    
    
    //Mapping from Entity to DTO
    private BlogDTO convertDTO(Blog blogFromDB){
        
        BlogDTO blogDTO = new BlogDTO();
        blogDTO.setAuthor(blogFromDB.getAuthor());
        blogDTO.setContent(blogFromDB.getContent());
        blogDTO.setTitle(blogFromDB.getTitle());
        
        return blogDTO;
        
    }

    //Mapping from DTO to Entity
    private Blog mappingBlogEntity(BlogDTO blogDTO) {

        Blog blogEntity = new Blog();

        blogEntity.setContent(blogDTO.getContent());
        blogEntity.setTitle(blogDTO.getTitle());
        blogEntity.setAuthor(blogDTO.getAuthor());

        return blogEntity;
    }
}
