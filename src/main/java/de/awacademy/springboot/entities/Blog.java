package de.awacademy.springboot.entities;

import lombok.Data;
import lombok.Generated;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Blog {

    @Id
    @GeneratedValue
    private Long id;

    private String author;
    private String title;
    private String content;

}
